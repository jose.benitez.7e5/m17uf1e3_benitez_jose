using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerName : MonoBehaviour
{
    private GameObject _player;
    private DataPlayer _playerData;

    public GameObject playername;
    // Start is called before the first frame update
    void Start()
    {
        _player = GameObject.Find("Female1");
        _playerData = _player.GetComponent<DataPlayer>();
        this.gameObject.GetComponent<Text>().text = _playerData.playername;
    }

    // Update is called once per frame
    void Update()
    {

    }
}