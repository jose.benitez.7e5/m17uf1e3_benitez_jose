using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Patrol : MonoBehaviour
{
    public float distance;
    public float origin;
    public float position;
    public float pointb;
    public int direction;
    public int waiting;
    public float switcher;
    // Start is called before the first frame update
    void Start()
    {
        origin = this.gameObject.GetComponent<Transform>().position.x;
        distance = this.gameObject.GetComponent<DataPlayer>().distance;
        position = origin;
        pointb = position - distance;
        waiting = 180;
    }

    // Update is called once per frame
    void Update()
    {


        //transform.position = new Vector3(this.gameObject.GetComponent<DataPlayer>().speed * Input.GetAxis("Horizontal") + transform.position.x, transform.position.y, transform.position.z);
        position = this.gameObject.GetComponent<Transform>().position.x;
        //transform.translate(Vector2.left * this.gameObject.GetComponent<DataPlayer>().speed * Time.deltaTime);
        if (pointb < transform.position.x&&transform.position.x-pointb>0.01) {
            direction = -1;
        }
        else if (pointb > transform.position.x&&pointb-transform.position.x>0.01)
        {
            direction = 1;
        }
        else if(pointb-transform.position.x<=0.01) 
        {
            direction = 0;
        }
            switch (direction)
        {
            case -1:
                transform.position = new Vector3(-this.gameObject.GetComponent<DataPlayer>().speed*Time.deltaTime+ transform.position.x, transform.position.y, transform.position.z);
                break;
            case 0:
                waiting--;
                if(waiting==0)
                    {
                    if (this.gameObject.GetComponent<SpriteRenderer>().flipX == false)
                        {
                            this.gameObject.GetComponent<SpriteRenderer>().flipX = true;
                        }
                    else this.gameObject.GetComponent<SpriteRenderer>().flipX = false;
                    
                   
                    switcher = origin;
                    origin = pointb;
                    pointb = switcher;
                    waiting = 180;
                    }
                break;
            case 1:
                transform.position = new Vector3(this.gameObject.GetComponent<DataPlayer>().speed*Time.deltaTime + transform.position.x, transform.position.y, transform.position.z);
                break;
        }
    }
}