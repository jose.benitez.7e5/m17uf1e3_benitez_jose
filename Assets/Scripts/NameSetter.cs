using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class NameSetter : MonoBehaviour
{
    private string _input;
    public Text _playerDataName;
    // Start is called before the first frame update
    void Start()
    {
        //_input = _playerDataName.GetComponent<Text>().text;
        //PlayerPrefs.SetString("name", _input);
        //SceneManager.LoadScene(0);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OtherScene()
    {
        _input = _playerDataName.text;
        PlayerPrefs.SetString("name", _input);
        SceneManager.LoadScene(1);
    }
}
