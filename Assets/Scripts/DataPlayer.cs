using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public enum Guild
{
    Warrior,
    Mage,
    Druid,
    Cultist,
    Monk,
    Priest,
    Berserk,
    Joker,
    Thief,
    Paladin,
}
public class DataPlayer : MonoBehaviour
{
    public string playername;


    public float height;

    public Guild currentGuild;

    public float distance =4;

    public float weight = 50f;
    public float agility;
    public float speed;
    public Sprite[] Female;
    int frame = 0;
    int femalei;

    // Start is called before the first frame update
    void Start()
    {
        currentGuild = Guild.Druid;
        speed = 50f / weight;
        height=1.6F;
        playername=PlayerPrefs.GetString("name");
}

    // Update is called once per frame
    void Update()
    {
        speed = 50f / weight;
       
        this.gameObject.GetComponent<SpriteRenderer>().sprite = Female[frame];
        femalei++;
        if (femalei == 120)
        {
            frame++;
            femalei = 0;
        }
        if (frame == Female.Length - 1) frame = 0;

    }
}
