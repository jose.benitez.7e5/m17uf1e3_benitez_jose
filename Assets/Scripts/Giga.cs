using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Giga : MonoBehaviour
{
    public bool normal;
    public bool gettingbigger;
    public bool giant;
    public bool gettingnormal;
    public bool cooldownrunning;
    public bool ready;
    public float cooldown;
    public float duration;
    public float increase;
   
   
    // Start is called before the first frame update
    void Start()
    {
        normal = true;
        giant = false;
        cooldown = 180;
        duration = 180;
        increase = this.gameObject.GetComponent<DataPlayer>().height;
        ready = true;
        gettingbigger = false;
        gettingnormal = false;
        cooldownrunning = false;
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.Space)&&ready==true&&normal==true)
        {
            ready = false;
            normal = false;
            gettingbigger = true;
        }
        if (gettingbigger == true&&duration==180)
        {
            transform.localScale = new Vector3(increase*0.0001f + transform.localScale.x, increase * 0.0001f + transform.localScale.y, transform.localScale.z);
            this.gameObject.GetComponent<DataPlayer>().height += 0.0001f;
            this.gameObject.GetComponent<DataPlayer>().weight += 0.01f;
        }

        if (this.gameObject.GetComponent<DataPlayer>().height >= 3)
        {
            gettingbigger = false;
            giant = true;

        }
        if (giant == true)
        {
            duration-=0.01f;
        }
        if (duration <= 0)
        {
            giant = false;
            gettingnormal = true;
            duration = 180;
        }
        if (gettingnormal == true)
        {
            transform.localScale = new Vector3(-increase * 0.0001f + transform.localScale.x, -increase * 0.0001f + transform.localScale.y, transform.localScale.z);
            this.gameObject.GetComponent<DataPlayer>().height -=0.0001f;
            this.gameObject.GetComponent<DataPlayer>().weight -= 0.01f;
        }
        if (this.gameObject.GetComponent<DataPlayer>().height <= increase && ready==false)
        {
            normal = true;
            gettingnormal = false;
            cooldownrunning = true;
            
        }
        if (cooldownrunning == true)
        {
            cooldown -= 0.01F;
        }
        if (cooldown <= 0&&gettingnormal==false)
        {
            cooldown = 180;
            ready = true;
            cooldownrunning = false;

        }

    }

    
}
