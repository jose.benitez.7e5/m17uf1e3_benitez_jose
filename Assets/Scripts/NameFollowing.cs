using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NameFollowing : MonoBehaviour
{
    string _name;
    private DataPlayer dataPlayer;
    private GameObject _player;
    public DataPlayer _playerData;
    
    // Start is called before the first frame update
    void Start()
    {
        _player= GameObject.Find("Female1");
        _playerData = _player.GetComponent<DataPlayer>();

        this.gameObject.GetComponent<Text>().text = PlayerPrefs.GetString("name") + ": " + _playerData.currentGuild.ToString();

    }

    // Update is called once per frame
    void Update()
    {
        transform.position = new Vector3(_playerData.transform.position.x, 0.5f +_playerData.transform.position.y, _playerData.transform.position.z);
    }
}
